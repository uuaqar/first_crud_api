
var express = require("express");
var bodyParser = require("body-parser");

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/myapp');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("Mongo DB Sucessfully connected.");
});

// var Employee = require("./api/restapi");


app.get('/', function(req, res) {
    res.json({"message": "This is my first app for restful API using express and MongoDB."})
});

require('./routes/routes.js')(app);
require('./routes/userAccounts.route.js')(app);

app.listen(3000, function(){
    console.log("Server is listning on port 3000")
});




