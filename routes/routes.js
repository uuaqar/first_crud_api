module.exports = function(app) {
var employee = require("../controller/mycontroller.js");

app.post('/addemployee', employee.create);
app.get('/employee', employee.findAll);
app.get('/employee/:empId', employee.findOne);
app.put('/employee/:empId', employee.update);
app.delete('/employee/:empId', employee.delete);
}
