var userAccounts = require("../models/userAccounts.model.js");
var pushNotifications = require("../models/pushNotifications.model.js");

exports.create = function(req, res) {

    let userAccount = new userAccounts
    let userObj = [];
    let data;
    let TotalUsers = 1000;

    for (let value = 1; value <= TotalUsers; value++ ) {
        
        let deviceType
        let user = "User";
        let token = "Token";
        let check = value;

        if (check % 2) { 
            deviceType = 0
        } else {
            deviceType = 1
        }

         userAccount = new userAccounts ({userName: `${user}${value}`,
             device_token: `${token}${value}`, 
             device_type: deviceType });
            
            userAccount.save(function(err, data){

            if(err){
                console.log(err);
                res.status(500).send({message:'error while creating User Account.'})
                return;
            } else{
               // res.end(data);
            }
            
        });
        
    }

    res.send(`${TotalUsers} user(s) Added successfuly.`);    
};


var readUsers = function(usersArray) {

    function registerDevice (user) {

        return new Promise (function(resolve, reject){

            setTimeout(function(){
                let localplatformARN = `platFormARN${user._id}`

                let pushNotification = new pushNotifications ({device_token: user.device_token,
                device_type: user.device_type,
                platformARN: localplatformARN });

                pushNotification.save(function(err, data){
                    if(err){
                        reject();
                    } else {
                        resolve(console.log(`Record for ${user.userName} inserted successfuly in pushNotifiction.`));
                        return localplatformARN
                    }
                });
            }, 500)
            
        })
    }

    for(var i=0;i<usersArray.length;i++) {
        registerDevice(usersArray[i]).then();
    }
        
}
exports.portusers = function(req, res) {
	userAccounts.find().lean().then (function(users) {
		if(!users || !users.length) {
			console.log('error while reading User Account.');		
		} else {
           readUsers(users);		
		}
    }, function(err){
        console.log(err) 
    }
);
}

