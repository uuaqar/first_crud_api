var Employee = require("../models/employee.js");

exports.create = function(req, res) {
    if(!req.body.name){
        res.status(400).send({message:'Employee Name cannot be empty.'})
    }

    var employee = new Employee ({name: req.body.name, address: req.body.address, salary: req.body.salary });

    employee.save(function(err, data){
        console.log(data);
        if(err){
            console.log(err);
            res.status(500).send({message:'error while creating employee.'})
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Employee.find(function(err,employee){
        if(err) {
        res.status(500).send({message:'Error occurd while retreiving employee.'})
        } else {
            res.send(employee)
        }
    });
};

exports.findOne = function(req, res) {
    Employee.findById(req.params.empId, function(err,employee){
        if(err) {
        res.status(500).send({message:'Error occurd while retreiving employee ' + req.params.empId +'.'})
        } else {
            res.send(employee)
        }
    })
}

exports.update = function(req, res) {

    Employee.findById(req.params.empId, function(err, employee){
        if(err) {
        res.status(500).send({message:'Error occurd while retreiving employee ' + req.params.empId +'.'})
        }
    });

    employee.name = req.body.name;
    employee.address = req.body.address;
    employee.salary = req.body.salary;

    employee.save(function(err,data){
        console.log(data);
        if(err){
            res.status(500).send({message: `error while updating employee record ${req.params.empId}` })
        } else {
            res.send(data)
        }
    })
}

exports.delete = function(req, res) {
    Employee.remove(req.params.empId, function(err, data){
        console.log(data);
        if(err){
            res.status(500).send({message: `Error while deleting employee record ${req.params.empId}` })
        } else {
            res.send({message: `Employee ${req.params.empId} deleted successfully.`})
        }
    } )
}