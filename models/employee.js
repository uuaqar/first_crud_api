var mongoose = require("mongoose");

    var EmployeeScema = mongoose.Schema({
    name: String,
    address: String,
    salary: String,
})

module.exports = mongoose.model('Employee', EmployeeScema)