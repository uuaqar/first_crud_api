var mongoose = require("mongoose");

    var pushNotifications = mongoose.Schema({
    platformARN: String,
    device_token: String,
    device_type: Number,
})

module.exports = mongoose.model('pushNotifications', pushNotifications)