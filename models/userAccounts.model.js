var mongoose = require("mongoose");

    var userAccounts = mongoose.Schema({
    userName: String,
    device_token: String,
    device_type: Number,
})

module.exports = mongoose.model('userAccounts', userAccounts)